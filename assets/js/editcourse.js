document.addEventListener('DOMContentLoaded', function () {
    const id = window.location.search.split('=')[1]
    fetch('http://127.0.0.1:5000/kursus/' + id + '/', {
        method: 'GET'
    })
        .then(response => response.json())
        .then(response => {
            const title = document.querySelector('#updatenamecourse')
            title.value = response.nama_kursus
            const cat = document.querySelector('#updatecategory')
            cat.value = response.kategori
            const desc = document.querySelector('#updatedesc')
            desc.value = response.deskripsi
            const content = document.querySelector('#updatecontent')
            content.value = response.konten
            const requir = document.querySelector('#updaterequir')
            requir.value = response.syarat
        })

    const btnupdate = document.querySelector('#btn-update')
    btnupdate.addEventListener('click', (e) => {
        e.preventDefault()
        const editform = document.querySelector('#editcourse')
        let data = new FormData(editform)
        let data_input = Object.fromEntries(data)

        if (data_input.nama_kursus == '') {
            delete data_input.nama_kursus
        }
        if (data_input.kategori == '') {
            delete data_input.kategori
        }
        if (data_input.deskripsi == '') {
            delete data_input.deskripsi
        }
        if (data_input.konten == '') {
            delete data_input.konten
        }
        if (data_input.syarat == '') {
            delete data_input.syarat
        }
        if (Object.entries(data_input).length === 0) {
            return alert('blank form')
        }

        data_input = JSON.stringify(data_input)
        // console.log(data_input)

        fetch('http://127.0.0.1:5000/kursus/' + id + '/', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: data_input,
            credentials: "same-origin"
        })
            .then(response => response.json())
            .then(response => {
                console.log(response)
                alert(response['sukses'])
                window.location.href = 'adminindex.html'
            })
    })
})