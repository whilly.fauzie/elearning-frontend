document.addEventListener('DOMContentLoaded', function () {
    const btnadd = document.querySelector('#btn-add')
    btnadd.addEventListener('click', (e) => {
        e.preventDefault()
        const editform = document.querySelector('#addcourse')
        let data = new FormData(editform)
        let data_input = Object.fromEntries(data)

        if (data_input.nama_kursus == '' || data_input.kategori == '' || data_input.deskripsi == '' || data_input.konten == '' || data_input.syarat == '' || data_input.tutor_id == '') {
            return alert("blank form")
        }
        
        // if (Object.entries(data_input).length === 0) {
        //     return alert('blank form')
        // }

        data_input = JSON.stringify(data_input)
        // console.log(data_input)

        fetch('http://127.0.0.1:5000/kursus/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: data_input,
            credentials: "same-origin"
        })
            .then(response => response.json())
            .then(response => {
                console.log(response)
                alert(response['sukses'])
                window.location.href = 'adminindex.html'
            })
    })
})