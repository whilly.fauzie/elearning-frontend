document.addEventListener('DOMContentLoaded', function () {
    a = localStorage.emailtutor

    fetch('http://127.0.0.1:5000/tutors/profile/', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            email: a
        },
        credentials: "same-origin"
    })
        .then(response => response.json())
        .then(response => {
            const name = document.querySelector('#updatename')
            name.value = response.nama
            const email = document.querySelector('#updateemail')
            email.value = response.email
        })

    const btnupdate = document.querySelector('#btn-update')
    btnupdate.addEventListener('click', (e) => {
        e.preventDefault()
        const editform = document.querySelector('#editprofile')
        let data = new FormData(editform)
        let data_input = Object.fromEntries(data)

        if (data_input.nama == '') {
            delete data_input.nama
        }
        if (data_input.email == '') {
            delete data_input.email
        }
        if (Object.entries(data_input).length === 0) {
            return alert('blank form')
        }

        data_input = JSON.stringify(data_input)
        // console.log(data_input)

        fetch('http://127.0.0.1:5000/tutors/profile/', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                email: a
            },
            body: data_input,
            credentials: "same-origin"
        })
            .then(response => response.json())
            .then(response => {
                // console.log(response)
                localStorage.emailtutor = response['email']
                alert(response['sukses'])
                window.location.href = 'adminindex.html'
            })
    })
});