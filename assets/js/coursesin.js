document.addEventListener('DOMContentLoaded', function () {
    fetch('http://127.0.0.1:5000/kursus/', {
        method: 'GET'
    })
        .then(response => response.json())
        .then(response => {
            // console.log(response)
            const courses = document.querySelector("#ContentIndex");
            let emptystr = "";
            response.forEach(post => {
                // console.log(post)
                emptystr += `
                <div class="col-lg-4">
                    <div class="card-deck">
                        <div class="card" id="cardcontent">
                            <img class="card-img-top" src="../assets/images/${post.kursus_id}.jpg" alt="Card image cap">
                            <div class="card-body">
                            <h5 class="card-title">${post.nama_kursus}</h5>
                                <a href="detailkursusin.html?kid=${post.kursus_id}" class="card-text"><small class="text-muted">See detail..</small></a>
                            </div>
                        </div>
                    </div>
                </div>
                `;
            });
            courses.innerHTML = emptystr;
        });
})