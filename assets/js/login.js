document.addEventListener('DOMContentLoaded', function () {
    const btn_login = document.querySelector('#btn-login')
    btn_login.addEventListener('click', (e) => {
        e.preventDefault()
        const form = document.querySelector('#inputlogin') // ambil form
        let data = new FormData(form) // membuat form kosong
        let data_input = Object.fromEntries(data) // ambil data dari form dan diubah menjadi dict untuk dimasukan ke api
        // console.log(data_input)    

        if (data_input.email == '' || data_input.password == '') {
            return alert('enter email and password')
        }
        let encd = window.btoa(`${data_input.email}:${data_input.password}`) //merubah enkripsi
        let auth = `Basic ${encd}`

        fetch('http://127.0.0.1:5000/login/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                authorization: auth
            },
            credentials: "same-origin"
        })
            .then(response => response.json())
            .then(response => {
                // console.log(response)
                if (response['email']) {
                    localStorage.emailsiswa = response['email']
                    // alert("login succesfull")
                    window.location.href = 'indexin.html'
                } else {
                    return alert(response['error'])
                }
            })
    })
})