document.addEventListener('DOMContentLoaded', function () {
    const btn_login = document.querySelector('#btn-login')
    btn_login.addEventListener('click', (e) => {
        e.preventDefault()
        const form = document.querySelector('#inputlogin')
        let data = new FormData(form)
        let data_input = Object.fromEntries(data)
        // console.log(data_input)

        if (data_input.email == '' || data_input.password == '') {
            return alert('enter email and password')
        }
        let encd = window.btoa(`${data_input.email}:${data_input.password}`) //merubah enkripsi
        let auth = `Basic ${encd}`

        fetch('http://127.0.0.1:5000/adminlogin/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                authorization: auth
            },
            credentials: "same-origin"
        })
            .then(response => response.json())
            .then(response => {
                // console.log(response)
                if (response['email']) {
                    localStorage.emailtutor = response['email']
                    // alert("login succesfull")
                    window.location.href = 'adminindex.html'
                } else {
                    return alert(response['error'])
                }
            })
    })
})