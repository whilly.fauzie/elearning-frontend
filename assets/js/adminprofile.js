document.addEventListener('DOMContentLoaded', function () {
    a = localStorage.emailtutor
    
    fetch('http://127.0.0.1:5000/tutors/profile/', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            email: a
        },
        credentials: "same-origin"
    })
        .then(response => response.json())
        .then(response => {
            const name = document.querySelector('#view-name')
            name.innerHTML = response.nama
            const email = document.querySelector('#view-email')
            email.innerHTML = response.email
        })
});