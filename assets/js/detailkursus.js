document.addEventListener('DOMContentLoaded', function () {
  const id = window.location.search.split('=')[1]
  fetch('http://127.0.0.1:5000/kursus/' + id + '/', {
    method: 'GET'
  })
    .then(response => response.json())
    .then(response => {
      const title = document.querySelector('#title')
      title.innerHTML = response.nama_kursus
      const desc = document.querySelector('#desc')
      desc.innerHTML = response.deskripsi
      const content = document.querySelector('#content')
      content.innerHTML = response.konten
      const requir = document.querySelector('#requir')
      requir.innerHTML = response.syarat
    })
  const imagecard = document.querySelector('#cardImg')
  const img = imagecard.querySelector('img')
  img.src = `../assets/images/${id}.jpg`
});