document.addEventListener('DOMContentLoaded', function () {
    const btn_sign = document.querySelector('#btn-sign')
    btn_sign.addEventListener('click', (e) => {
        e.preventDefault()
        const inputform = document.querySelector('#inputsign')
        let data = new FormData(inputform)
        let data_input = Object.fromEntries(data)

        if (data_input.nama == "" || data_input.email == "" || data_input.password == "") {
            return alert("blank form")
        }

        data_input = JSON.stringify(data_input)
        // console.log(data_input)

        fetch('http://127.0.0.1:5000/users/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: data_input
        })
            .then(response => response.json())
            .then(response => {
                //if (response["sukses"]) {
                alert(response['sukses'])
                window.location.href = 'login.html'
                // } else {
                // return alert(response["error"])
                // }
            })
    })
})