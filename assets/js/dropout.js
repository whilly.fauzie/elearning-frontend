document.addEventListener('DOMContentLoaded', function () {
    const btn_enroll = document.querySelector('#btnDrop')
    btn_enroll.addEventListener('click', (e) => {
        e.preventDefault()

        let data = {
            kursus_id: window.location.search.split('=')[1],
            emailsiswa: localStorage.emailsiswa
        }
        data = JSON.stringify(data)

        fetch('http://127.0.0.1:5000/dropout/', {
            method: 'DELETE',
            headers: { 'Content-Type': 'application/json' },
            body: data,
            credentials: "same-origin"
        })
            .then(response => response.json())
            .then(response => {
                if (response['message'] == 'success') {
                    window.location.href = 'coursesin.html'
                } else {
                    return alert(response['message'])
                }
            })
    })
})