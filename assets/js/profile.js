document.addEventListener('DOMContentLoaded', function () {
    a = localStorage.emailsiswa
    
    fetch('http://127.0.0.1:5000/users/profile/', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            email: a
        },
        credentials: "same-origin"
    })
        .then(response => response.json())
        .then(response => {
            const name = document.querySelector('#view-name')
            name.innerHTML = response.nama
            const email = document.querySelector('#view-email')
            email.innerHTML = response.email
        })
});